import React, { Component } from 'react';
import ChatCard from '../chat-card/ChatCard';
import ChatFab from '../chat-fab/ChatFab';
import { connect } from 'react-redux';
import { sendMessage, connectChatClient, getChatConfig } from '../actions'
import PropTypes from 'prop-types';
import { APP_DOMAIN, XMPP_URL, XMPP_COMPONENT, XMPP_AUTH_URL } from '../config';

const xmppUri = XMPP_URL
const componentName = XMPP_COMPONENT;
const authUri = XMPP_AUTH_URL;
const domain = `${APP_DOMAIN}`;

class Chat extends Component {
  constructor(props) {
    super(props);

    this.handleChatToggle = this.handleChatToggle.bind(this);
  }

  componentDidMount() {
    this.props.connectChatClient(xmppUri, componentName, authUri, domain);
    this.props.getChatConfig();
  }

  render() {
    return (
      <div className={"ChatbotProj main-chat"
        + " "
        + (this.props.isMobile ? 'mobile' : '')
        + " "
        + (this.props.openChat ? 'open-chat' : 'closed-chat')
      }>
        {this.props.openChat ?
          <ChatCard
            onClose={() => this.handleChatToggle(false)}
            onSendMessage={this.props.sendMessage}
            messages={this.props.messages}
            agent={this.props.agent}
          />
          :
          <ChatFab onClick={() => this.handleChatToggle(true)} />
        }
      </div>
    );
  }

  handleChatToggle(opened) {
    this.props.onToggleChat && this.props.onToggleChat(opened);
  }
}

Chat.propTypes = {
  messages: PropTypes.array.isRequired,
  onToggleChat: PropTypes.func,
  openChat: PropTypes.bool.isRequired,
  isMobile: PropTypes.bool
}

const mapStateToProps = (state) => ({
  messages: state.chatMessages.messages,
  agent: state.chatConfig.agent,
})
const mapDispatchToProps = {
  sendMessage,
  connectChatClient,
  getChatConfig,
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
