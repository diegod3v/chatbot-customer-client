import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension';

const isInProduction = process.env.NODE_ENV === 'production';
const loggerMiddleware = createLogger();

const initialState = {}
const enhancers = []
const middleware = [
    thunk,
    !isInProduction && loggerMiddleware,
].filter(Boolean)

if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__

    if (typeof devToolsExtension === 'function') {
        enhancers.push(devToolsExtension())
    }
}

const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
)

const configureStore = () => (createStore(
    rootReducer,
    initialState,
    composedEnhancers
))

export default configureStore