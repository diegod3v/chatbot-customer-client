const chatContextApi = {
  getContext: () => {
    const contextStore = window.sessionStorage.getItem('chatbotproj_context');
    if (contextStore) {
      return JSON.parse(contextStore);
    } else {
      window.sessionStorage.setItem('chatbotproj_context', '{}');
    }
  },

  getContextVar: (name) => {
    const contextStore = window.sessionStorage.getItem('chatbotproj_context');
    if (contextStore) {
      const context = JSON.parse(contextStore);
      return context[name];
    } else {
      window.sessionStorage.setItem('chatbotproj_context', '{}');
      return undefined;
    }
  },

  setContextVar: (name, val) => {
    const contextStore = window.sessionStorage.getItem('chatbotproj_context');
    const context = contextStore ? JSON.parse(contextStore) : {}

    context[name] = val;

    const nextContextSerialized = JSON.stringify(context);
    window.sessionStorage.setItem('chatbotproj_context', nextContextSerialized);

    return context;
  }
}

export default chatContextApi;