import React, { Component } from 'react';
import logo from '../logo.svg';
import ChatMessageTemplate from './ChatMessageTemplate';


class ChatMessage extends Component {
  render() {
    const { isAgent, avatarSrc, text, attachment, onSendMessage } = this.props;
    return (
      <li className={"ChatMessage " + (isAgent ? "ChatMessage-agent-message" : "ChatMessage-customer-message")}>
        {isAgent && avatarSrc ?
          <div className="ChatMessage-message-avatar">
            <img src={logo} alt="agent avatar" />
          </div>
          :
          null
        }
        <div className="ChatMessage-bubble">
          {attachment ? <ChatMessageTemplate attachment={attachment} onSendMessage={onSendMessage} /> : <p>{text}</p>}
        </div>
      </li>
    );
  }
}

export default ChatMessage;
