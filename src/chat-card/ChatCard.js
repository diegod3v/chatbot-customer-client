import React, { Component } from 'react';
import ChatTypewriter from './ChatTypewriter';
import ChatHeader from './ChatHeader';
import ChatConversationList from './ChatConversationList';
import PropTypes from 'prop-types';

class ChatCard extends Component {
  constructor(props) {
    super(props);
    this.handleOnSendMessage = this.handleOnSendMessage.bind(this);
  }
  render() {
    const { agent } = this.props;

    return (
      <div className="ChatCard">
        <ChatHeader
          name={agent.isAgent ? agent.agentName : agent.botName}
          title={agent.isAgent ? "Ejecutivo" : "Bot"}
          onCloseClick={this.props.onClose}
        />
        <div className="ChatCard-conversation">
          <ChatConversationList messages={this.props.messages} onSendMessage={this.handleOnSendMessage} />
        </div>
        <div className="ChatCard-typewriter">
          <ChatTypewriter onSendMessage={this.handleOnSendMessage} />
        </div>
      </div>
    );
  }

  handleOnSendMessage(message) {
    this.props.onSendMessage(message);
  }
}

ChatCard.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSendMessage: PropTypes.func.isRequired
}

ChatCard.defaultProps = {
  messages: []
}

export default ChatCard;
