import React, { Component } from 'react';

const ChatMessageTemplate = ({ attachment: { payload }, onSendMessage }) => (
  <div className='ChatMessageTemplate'>
    <p>{payload.text}</p>
    {payload.buttons && payload.buttons.length > 0 &&
      <div className='ChatMessageTemplate-buttons'>
        <ul>
          {payload.buttons.map((btn) => (
            <li>
              {btn.type === 'web_url' ?
                <a href={btn.url} target='_blank' rel="noreferrer">{btn.title}</a>
                :
                <button
                  onClick={e => onSendMessage({
                    ...btn.payload,
                  })}>
                  {btn.title}
                </button>
              }
            </li>
          ))}
        </ul>
      </div>
    }
  </div>
)

export default ChatMessageTemplate;
