import React, { Component } from 'react';
import logo from '../logo.svg';

class ChatHeader extends Component {
  render() {
    const { name, title } = this.props;
    return (
      <header className="ChatHeader">
        <div className="ChatHeader-avatar">
          <img src={logo} alt="agent avatar" />
        </div>
        <div className="ChatHeader-info">
          <h5>{name}</h5>
          <small>{title}</small>
        </div>
        <button className="ChatHeader-close"
          onClick={this.props.onCloseClick}
        >
        </button>
      </header>
    );
  }
}

export default ChatHeader;
