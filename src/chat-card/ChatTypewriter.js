import React, { Component } from 'react';

class ChatTypewriter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textAreaRows: 1,
      message: "",
    }
    this.handleMessageTyped = this.handleMessageTyped.bind(this);
    this.handleSubmitMessage = this.handleSubmitMessage.bind(this);
    this.handleTextAreaKeyDown = this.handleTextAreaKeyDown.bind(this);
  }

  render() {
    return (
      <div className="ChatTypewriter">
        <form
          className="ChatTypewriter-input"
          onSubmit={this.handleSubmitMessage}
          ref={(el) => this.messageForm = el}>
          <textarea
            name="message"
            rows={this.state.textAreaRows}
            placeholder="Escribe tu mensaje"
            maxLength="1200" type="text"
            autoCapitalize="sentences"
            autoCorrect="on"
            onChange={this.handleMessageTyped}
            value={this.state.message}
            onKeyDown={this.handleTextAreaKeyDown}
          />
          <button type="submit" className="btn-action"></button>
        </form>
      </div>
    );
  }

  handleMessageTyped(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmitMessage(event) {
    event.preventDefault();
    const { message } = this.state;

    if (message.length > 0) {
      this.props.onSendMessage(message);
      this.setState({ message: "" });
    }
  }

  handleTextAreaKeyDown(event) {
    if (event.key === 'Enter') {
      event.preventDefault();
      event.stopPropagation();
      this.handleSubmitMessage(event);
    }
  }
}

export default ChatTypewriter;