import React, { Component } from 'react';
import ChatMessage from './ChatMessage';


class ChatConversationList extends Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.messages.length > prevProps.messages.length) {
      this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
    }
  }

  componentDidMount() {
    this.messagesEnd.scrollIntoView();
  }

  render() {
    return (
      <ul className="ChatConversationList">
        {this.props.messages && this.props.messages.map((chatMessage, i, arr) => {
          if (chatMessage.isAgent) {
            const withAvatar = (!arr[i + 1] || !arr[i + 1].isAgent)
            return (<ChatMessage key={chatMessage.uuid} onSendMessage={this.props.onSendMessage} avatarSrc={withAvatar} {...chatMessage} />);
          }
          else return (<ChatMessage key={chatMessage.uuid} {...chatMessage} />);
        }
        )}
        <li style={{ float: "left", clear: "both" }}
          ref={(el) => { this.messagesEnd = el; }}>
        </li>
      </ul>
    );
  }
}

export default ChatConversationList;
