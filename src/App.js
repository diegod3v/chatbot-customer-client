import React, { Component } from 'react';
import css from './App.css';
import Chat from './chat/Chat';
import { Provider } from 'react-redux';
import configureStore from './Store';
import Frame from 'react-frame-component';
import URLSearchParams from 'url-search-params';

const store = configureStore();

class App extends Component {
  constructor(props) {
    super(props);

    this.mediaQuery = window.matchMedia('screen and (max-width: 767.98px)');

    this.state = {
      openChat: false,
    }

    this.handleToggleChat = this.handleToggleChat.bind(this);
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {
    this.updateDimensions(this.mediaQuery);
  }

  componentDidMount() {
    this.mediaQuery.addListener(this.updateDimensions);
  }

  componentWillUnmount() {
    this.mediaQuery.removeEventListener(this.updateDimensions);
  }

  render() {
    const chatStyles = {
      right: 0,
      bottom: 0,
      position: 'fixed',
      border: 'none',
      boxShadow: 'none',
    };

    const chatSize = this.state.openChat ?
      this.state.isMobile ?
        {
          bottom: '0 !important',
          right: '0!important',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
        }
        :
        { width: '25rem', height: '34rem' }
      :
      { width: '7rem', height: '7rem' };

    return (
      <Frame
        initialContent={`
        <!DOCTYPE html>
          <html>
            <head>
            <style type="text/css">
            ${(process.env.NODE_ENV === 'production' ? css : '')}
            </style>
              ${(!(process.env.NODE_ENV === 'production') ? document.head.innerHTML : '')}
            <meta name="viewport" content="width=device-width, initial-scale=1">
            </head>
            <body>
              <div id="main"></div>
            </body>
          </html>`}
        mountTarget='#main'
        style={{ ...chatStyles, ...chatSize }}
      >
        <Provider store={store}>
          <Chat onToggleChat={this.handleToggleChat} openChat={this.state.openChat} isMobile={this.state.isMobile} />
        </Provider>
      </Frame>
    );
  }

  handleToggleChat(opened) {
    this.setState({ openChat: opened });
  }

  updateDimensions(mediaQuery) {
    if (mediaQuery.matches && !this.state.isMobile) {
      this.setState({ isMobile: true });
    } else if (this.state.isMobile) {
      this.setState({ isMobile: false });
    }
  }
}

export default App;
