import { createAction } from 'redux-actions';

const CLIENT_MESSAGE = 'CLIENT_MESSAGE';
const AGENT_MESSAGE = 'AGENT_MESSAGE';

export const clientMessage = createAction(CLIENT_MESSAGE);
export const receiveMessage = createAction(AGENT_MESSAGE);
