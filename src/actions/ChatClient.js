import strophejs, { $msg, Strophe } from 'strophe.js';

if (process.env.NODE_ENV !== 'production') {
  window.strophe = strophejs;
  window.Shopify = { shop: 'fake.myshopify.com' }
}

const shop = window.Shopify.shop;

export class ChatClient {
  static instance;

  constructor(clientUrl, authUrl, domain) {
    if (this.instance) {
      return this.instance;
    }

    this.client = new Strophe.Connection(clientUrl);
    this.clientUrl = clientUrl;
    this.domain = domain;
    this.authUrl = authUrl;
    this.agentJid = null;
    this.instance = this;
  }

  async start() {

    const creds = await this.getCreds();

    try {
      this.client.connect(`${creds.username}@${this.domain}/main`, creds.password, (status) => {
        if (status === Strophe.Status.CONNECTED) {
          console.log('connected');
        } else if (status === Strophe.Status.DISCONNECTED) {
          console.log('disconnected');
        }
      });
    } catch (err) {
      console.log(err);
    }
  }

  async getCreds() {
    const username = localStorage.getItem('chatbotproj_username');
    const password = localStorage.getItem('chatbotproj_token');

    if (username || password) {
      return { username, password }
    }

    const nextToken = await fetch(this.authUrl, {
      body: JSON.stringify({ shop }),
      cache: 'no-cache',
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST',
      mode: 'cors',
    }).then(res => res.json());

    const { token } = nextToken;
    const tokenPayloadStr = atob(token.split('.')[1]);
    const { username: tokenUsername } = JSON.parse(tokenPayloadStr);

    localStorage.setItem('chatbotproj_username', tokenUsername);
    localStorage.setItem('chatbotproj_token', token);

    return { username: tokenUsername, password: token }
  }

  onMessage(callback) {
    this.client.addHandler((message) => {
      const type = message.getAttribute('type');
      const elems = message.getElementsByTagName('body');
      if (type === "chat" && elems.length > 0) {
        const body = elems[0];
        callback(Strophe.getText(body));
      }
      return true;
    }, null, 'message', null, null, null);
  }

  onIq(callback) {
    this.client.addHandler((iq) => {
      callback(iq);
      return true;
    }, null, 'iq', null, null, null);
  }

  onTransfer(callback) {
    this.onIq((iq) => {
      const elems = iq.getElementsByTagName('query');
      const jidUserTransferElms = elems[0].getElementsByTagName('jidUserTransfer');
      const jidUserTransferElm = jidUserTransferElms[0];
      if (jidUserTransferElm) {
        const jidUserTransfer = Strophe.getText(jidUserTransferElm);
        callback(jidUserTransfer);
      }
    })
  }

  send(stanza) {
    return new Promise((res, rej) => {
      try {
        this.client.send(stanza);
        res(stanza.nodeTree)
      } catch (err) {
        rej(err);
      }
    })
  }

  sendMessage(text) {
    const stanza = $msg({ to: this.agentJid, type: 'chat' })
      .c('body').t(text)

    return this.send(stanza).then(stanzaNode => {
      let type = stanzaNode.getAttribute('type');
      let elems = stanzaNode.getElementsByTagName('body');
      if (type === "chat" && elems.length > 0) {
        let body = elems[0];
        return Strophe.getText(body);
      }
    });
  }

}