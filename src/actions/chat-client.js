import { createAction } from 'redux-actions';
import { receiveMessage, clientMessage } from './messages';
import { setAgentInfo } from './chat-config';
import { ChatClient } from './ChatClient';
import chatContextApi from '../contextChat';

let chatbotProjClient;

const CLIENT_CONNECTING = 'CLIENT_CONNECTING';
const CLIENT_CONNECTED = 'CLIENT_CONNECTED';

export const clientConnecting = createAction(CLIENT_CONNECTING);
export const clientConnected = createAction(CLIENT_CONNECTED);

export const connectChatClient = (url, agentJid, authUrl, domain) => dispatch => {
  chatbotProjClient = new ChatClient(url, authUrl, domain);
  chatbotProjClient.agentJid = agentJid;

  dispatch(clientConnecting);

  chatbotProjClient.onMessage(message => {
    const objMessage = JSON.parse(message.replace(/&quot;/g, '"'));;
    const receivedMessage = { ...objMessage, isAgent: true }
    if (receivedMessage.nodeType === 'question') {
      console.log(receivedMessage);
      chatContextApi.setContextVar('questionNodeId', receivedMessage.currentNodeId);
    }
    dispatch(receiveMessage(receivedMessage));
  });

  chatbotProjClient.onTransfer((agentJid) => {
    chatbotProjClient.agentJid = `${agentJid}@${domain}/main`;
    dispatch(setAgentInfo({ isAgent: true }));
  })

  chatbotProjClient.start()
    .then(jid => {
      console.log(jid)
      dispatch(clientConnected);
    }).catch(err => {
      dispatch(clientConnected(err));
      console.log(err)
    });
}


export const sendMessage = message => dispatch => {
  const context = chatContextApi.getContext();

  let objMessage = {
    context
  };

  if (typeof message === 'string') {
    objMessage.text = message;
  } else {
    objMessage = { ...objMessage, ...message };
  }

  chatbotProjClient.sendMessage(JSON.stringify(objMessage))
    .then(messageText => {
      chatContextApi.setContextVar('questionNodeId', false);
      dispatch(clientMessage(objMessage))
    })
    .catch(err => {
      dispatch(clientMessage(err))
    })
}