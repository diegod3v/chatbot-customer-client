import { createAction } from 'redux-actions';
import { API_BASE_URL } from '../config';

const SET_AGENT_INFO = 'SET_AGENT_INFO';

export const setAgentInfo = createAction(SET_AGENT_INFO);

export const getChatConfig = () => dispatch => {
  const token = localStorage.getItem('chatbotproj_token');

  const config = fetch(`${API_BASE_URL}/chat/config`, {
    headers: {
      'Authorization': token,
    }
  })
    .then(res => res.json());

  config.then(({ botName, agentName }) => {
    dispatch(setAgentInfo({ botName, agentName, isAgent: false }));
  });
};
