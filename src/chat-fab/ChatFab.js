import React, { Component } from 'react';

class ChatFab extends Component {

    render() {
        return (
            <div className="ChatFab">
                <button className="ChatFab-main-action" onClick={this.props.onClick}>
                    <i className="bubbles-icon"/>
                </button>
            </div>
        );
    }
}

export default ChatFab;
