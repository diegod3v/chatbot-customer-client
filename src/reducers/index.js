import { combineReducers } from 'redux'
import chatMessages from './messages';
import chatConfig from './chat-config';

export default combineReducers({
  chatMessages,
  chatConfig,
})