import { handleActions } from "redux-actions";
import { clientMessage, receiveMessage } from "../actions";

const chatMessagesReducer = handleActions({
  [clientMessage]: {
    next(state, action) {
      return {
        ...state,
        messages: [...state.messages, action.payload]
      }
    },
    throw(state, action) {
      return state;
    }
  },

  [receiveMessage]: {
    next(state, action) {
      return {
        ...state,
        messages: [...state.messages, action.payload]
      }
    },
    throw(state, action) {
      return state;
    }
  }
}, { messages: [] });

export default chatMessagesReducer;