import { handleActions } from "redux-actions";
import { setAgentInfo } from "../actions";

const chatConfigReducer = handleActions({
  [setAgentInfo]: {
    next(state, action) {
      return {
        ...state,
        agent: { ...state.agent, ...action.payload },
      }
    },
    throw(state, action) {
      return state;
    }
  },
}, { agent: {} });

export default chatConfigReducer;