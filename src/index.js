import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

let mountEl;
const rootEl = document.getElementById('root-chatbotproj');

if (rootEl) {
  mountEl = rootEl
} else {
  const elem = document.createElement('div');
  elem.id = 'root-chatbotproj';
  document.body.appendChild(elem);

  mountEl = elem;
}

ReactDOM.render(<App />, mountEl);
registerServiceWorker();
